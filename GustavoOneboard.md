Onboarding Gustavo Rodarte - 7 de agosto de 2017
===================

Data e hora: 07/08/2017, 09:00 às 10:00
Participantes: Felipe Miranda (FM) e Gustavo Rodarte (MS)


Valores e forma de trabalho
-------------

#### Valores: https://www.cloudia.com.br/sobre-nos/

#### Forma de trabalho: ritmo próprio, com muita liberdade e cobrança por resultado.

#### Metodologia de gerência de projetos: SCRUM. Planejamento semanal de sprints com quadros no Trello.

#### Vídeo de SCRUM: https://www.youtube.com/watch?v=XfvQWnRgxG0

#### Material de SCRUM: https://www.slideshare.net/srikanthps/scrum-in-15-minutes-presentation?qid=c7341a38-8559-410c-875e-28138f37fc5e&v=&b=&from_search=7

#### Nossos quadros no Trello:

##### Backlog:
https://trello.com/b/Z5Ns4pN2/%C3%A1rea-de-membros-backlog-do-produto

##### Sprint:
https://trello.com/b/cvkmd78O/%C3%A1rea-de-membros-sprint


####Estimativa dos dias e horários a trabalhar:

> **Horários:**

> Seg - das 09:00 às 13:00, 13:30 às 15:30
Ter - das 14:00 às 20:00
Qua - das 09:00 às 13:00, 13:30 às 15:30
Qui - das 14:00 às 20:00
Sex - das 09:00 às 13:00, 13:30 às 15:30




####Controle do horário: Toggl
Controlar as horas de trabalho no Toggl

####Projetos: use a criatividade para dividir os projetos (são todos dentro de tecnologia, provavelmente)

Lembrar do NDA


Entendendo a experiência:
-------------

Para entender a experiência dos pacientes e dos profissionais, é necessário fazer 2 coisas:

Entre em https://m.me/cloudiabot e fale com a Cloudia
Entre em cloudia.com.br/login, faça uma nova conta e veja as configurações que você pode fazer.

Área de Membros
-------------
A Área de Membros (cloudia.com.br/login) é um sistema Web, com front-end desenvolvido utilizando frameworks JavaScript populares e back-end desenvolvido utilizando NodeJS.

Utilizamos banco de dados relacional MySQL.

Tanto o banco de dados (RDS) quanto a máquina Linux onde a Cloudia é executada (EC-2) ficam na Amazon Web Services (AWS).

A parte técnica será explicada pelo Micael para você logo em seguida.

Modelo de dados:
-----------

O nosso modelo de dados pode ser consultado no esquemático de banco do MySQL Workbench.

Para o interfaceamento com o Python, utilizamos o framework SQLalchemy, que fornece ORMs e facilita muito o trabalho. O arquivo cloudia_mappings.py tem as classes que mapeiam os nossos dados do banco de dados.

De uma forma geral, é importante entender a seguinte estrutura, das clínicas às agendas:

Uma Clínica tem uma ou mais unidades (ou subsidiárias).

Uma unidade (ou subsidiária) tem um ou mais profissionais que trabalham nela. Um profissional pode trabalhar em uma ou mais unidades. 

Um profissional tem um ou mais tipos de consulta (que chamamos de calendário)

>Clínica <- unidade <- profissional <- tipos de consulta

Do lado do paciente, ele tem vários dados que o descrevem (ver tabela customer). O que liga um paciente a uma clínica é um facebook_messenger_id. Cada paciente, quando interage com uma página profissional diferente do Facebook, recebe um id diferente. Sempre que um paciente fala com uma nova página, ele tem que digitar o seu e-mail e outros dados. Usamos o e-mail como chave primária dos pacientes.


No nosso novo banco, existe o conceito de plano de saúde. O paciente tem um plano de saúde ligado a ele. As clínicas podem aceitar um ou vários planos de saúde. As unidades abaixo de uma clínica podem aceitar todos os planos de saúde aceitos pela clínica ou podem aceitar apenas uma parte deles. Da mesma forma, os profissionais dentro de uma clínica, podem aceitar todos os planos aceitos pela clínica ou uma parte deles.

As consultas (tabela appointment) são o que ligam os pacientes aos nossos clientes. Elas representam um compromisso marcado pelos clientes dos nossos clientes com os nossos clientes (profissionais de saúde).

Há no banco o conceito de FAQQuestions (perguntas frequentes), onde armazenamos as perguntas frequentes das clínicas, que podem ser 100% personalizáveis por eles na nossa área de membros. As perguntas frequentes podem ser organizadas em até 3 níveis com até 8 tópicos (e respostas para esses tópicos).

Há no banco também o conceito de assistentes (para cadastrar secretárias de profissionais), que não estamos usando muito.

(Ver esquemático SQL)
