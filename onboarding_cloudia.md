
# Cloudia

A Cloudia foi toda desenvolvida em Python.
Utilizamos banco de dados relacional MySQL.

Tanto o banco de dados (RDS) quanto a máquina Linux onde a Cloudia é executada (EC-2) ficam na AWS.


## Valores e forma de trabalho

Nossos valores se encontram [nesse link](https://www.cloudia.com.br/sobre-nos/).

Forma de trabalho: ritmo próprio, com muita liberdade e cobrança por resultado.

Metodologia de gerência de projetos: SCRUM. Planejamento semanal de sprints com quadros no Trello.

## Entendendo a experiência:

Para entender a experiência dos pacientes e dos profissionais, é necessário fazer 2 coisas:

- Entre em contato com a Cloudia [nesse link](https://m.me/cloudiabot).
- [Entre aqui](cloudia.com.br/login), faça uma nova conta e veja as configurações que você pode fazer.
- Para entender o webchat, fale com a Cloudia no [nosso site](www.cloudia.com.br).

## Modelo de dados:

O nosso modelo de dados pode ser consultado no esquemático de banco do **MySQL** Workbench.

Para o interfaceamento com o **Python**, utilizamos o framework *SQLalchemy*, que fornece ORMs e facilita muito o trabalho. O arquivo cloudia_mappings.py tem as classes que mapeiam os nossos dados do banco de dados.

De uma forma geral, é importante entender a seguinte estrutura, das clínicas às agendas:

Uma Clínica tem uma ou mais unidades (ou subsidiárias).

Uma unidade (ou subsidiária) tem um ou mais profissionais que trabalham nela. Um profissional pode trabalhar em uma ou mais unidades. 

Um profissional tem um ou mais tipos de consulta (que chamamos de calendário)

Então:

`Clínica <- unidade <-> profissional <- tipos de consulta`

Do lado do paciente, ele tem vários dados que o descrevem (ver tabela customer). O que liga um paciente a uma clínica é um facebook_messenger_id. Cada paciente, quando interage com uma página profissional diferente do Facebook, recebe um id diferente. Sempre que um paciente fala com uma nova página, ele tem que digitar o seu e-mail e outros dados. Usamos o e-mail, CPF, FacebookAppId e, algumas vezes, o próprio telefone, como chaves primárias dos pacientes.


No nosso novo banco, existe o conceito de plano de saúde. O paciente tem um plano de saúde ligado a ele. As clínicas podem aceitar um ou vários planos de saúde. As unidades abaixo de uma clínica podem aceitar todos os planos de saúde aceitos pela clínica ou podem aceitar apenas uma parte deles. Da mesma forma, os profissionais dentro de uma clínica, podem aceitar todos os planos aceitos pela clínica ou uma parte deles.

As consultas (tabela appointment) são o que ligam os pacientes aos nossos clientes. Elas representam um compromisso marcado pelos pacientes com os nossos clientes (profissionais de saúde).

Há no banco o conceito de FAQQuestions (perguntas frequentes), onde armazenamos as perguntas frequentes das clínicas, que podem ser 100% personalizáveis por eles na nossa área de membros. As perguntas frequentes podem ser organizadas em até 3 níveis com até 8 tópicos (e respostas para esses tópicos).

A Cloudia tem uma funcionalidade de responder automaticamente a comentários deixados por usuários em posts, tanto no próprio post como também já no inbox do usuário. Para entender com facilidade essa funcionalidade, deixe um comentário com o seu usuário aqui nesse post: https://web.facebook.com/permalink.php?story_fbid=217485568826270&id=178593926048768&_rdc=1&_rdr . Veja que você vai receber respostas automáticas tanto no post quando no inbox. A tabela postswithautomaticreplies contém os posts das várias clínicas onde os usuários devem ser respondidos automaticamente ao deixarem comentários. As respostas automáticas que devem ser enviadas a esses usuários ficam armazenadas na tabela automaticreply. Todos os comentários deixados por usuários em posts com respostas automáticas são armazenados na tabela commentonpost.

Tem 2 tabelas na Cloudia que dizem respeito ao armazenamento das conversas que os pacientes têm com o chatbot. São as tabelas servicereport e receivedmessage. Na tabela servicereport, nós resumimos o atendimento de um paciente em uma clínica, dizendo o número de mensagens que o paciente enviou, dessas, quantas mensagens foram entendidas, o horário que o atendimento iniciou e encerrou, entre outras. A tabela receivedmessage contém todas as mensagens trocadas entre pacientes e Cloudias das clínicas. Todo o histórico de mensagens trocadas está nela.

(Ver esquemático SQL)


## Fluxo das informações no código

Nosso código está no [Bit Bucket](https://bitbucket.org/felipe_cloudia/cloudia4clinics).

`git clone https://...@bitbucket.org/felipe_cloudia/cloudia4clinics.git`

O arquivo executável do nosso código é o scripts/bot_server.py. Nele, apenas criamos uma coleção de chatbots (bot/CollectionOfBots.py) com todos os clientes que estão ativos no nosso banco de dados (ou seja, aqueles que têm a flag isactive igual a 1).

Junto com isso, também colocamos para rodar em uma thread separada, o listador de compromissos (appointment_lister), que é a entidade responsável por enviar para os pacientes os lembretes de consultas dias antes dos compromissos marcados pelos pacientes, de acordo com a configuração de lembretes feita pela clínica na área de membros. E também envia para os profissionais um resumo da agenda deles todos os dias às 20:00.

O CollectionOfBots cria 2 dicionários com BotConsumers, 1 agrupando todos os bots dos nossos clientes no Facebook Messenger e outro dicionário agrupando todos os bots dos nossos clientes no site (webchat).

Já no CollectionOfBots, uma aplicação Flask é ativada e é aí, no método _index(self) onde chegam todas as mensagens recebidas pelas páginas dos nossos clientes no Facebook e repassadas pelo Facebook para o nosso webhook.

Neste método, detectamos se a mensagem recebida via post trata-se de uma mensagem de um usuário no Facebook messenger ou um comentário deixado em um post no Facebook, um comentário deixado em um post no Instagram, ou uma mensagem de um usuário via WebChat.

Nesta função, verifica-se se a mensagem que chegou do Facebook pertence a uma página que nós servimos. Se sim, nós enviamos essa mensagem para a função message_handler do BotConsumer dessa página (cada fan page do Facebook tem um BotConsumer distinto).

A função message_handler, do BotConsumer, verifica se aquele usuário específico que mandou aquela mensagem já existe em seu dicionário de conversas (dicionário de Talks). Se ele já existir, o método msg_received desse talk é chamado, com a mensagem recebida sendo repassada para ele. 

As conversas (Talk) são criadas 1 para cada usuário que entra em contato com a clínica e mantidas pelo BotConsumer por 10 minutos desde a última mensagem recebida. Depois de passados 10 minutos, esse Talk é descartado pelo BotConsumer. Uma melhoria potencial que podemos fazer é guardar o contexto da Cloudia no banco de dados, assim ela não esquece da conversa que teve anteriormente com o usuário.

O objeto do tipo Talk é quem controla uma conversa específica de um usuário com uma clínica específica. Um Objeto Talk utiliza um objeto da classe Context, que armazena todas as informações importantes sobre a conversa que um usuário está mantendo com uma clínica. O método process_msg da classe Talk é quem processa a mensagem recebida do usuário. Um objeto Talk controla o estado atual que a conversa se encontra, analisa a mensagem recebida do usuário e decide para qual próximo estado a conversa deve ir. 

## Estados da Cloudia

[Esse documento](https://drive.google.com/a/cloudia.com.br/file/d/0B-KdGeO_3-tWal85TGtTWkZXVGs/view?usp=sharing), no draw.io, tem uma documentação de todos os estados da Cloudia.

Os estados da Cloudia, a maioria deles documentados neste diagrama do draw.io acima, estão escritos como classes no diretório cloudia/state. O arquivo abstract_states.py contém as classes-base (abstratas dos estados).... Uma série de outras classes que merecem uma descrição melhor.

No método process_msg da classe Talk, você percebe a sequencia que deve ser seguida por um estado quando a máquina de estados chega nesse estado. Existe uma sequencia de métodos que são chamados no método process_msg, que as classes de estado no diretório cloudia/state devem implementar. 

## Integração com a Google Agenda:

A Cloudia marca consultas via API no Google Agenda do profissional servido por ela. Todas as funções da integração com a Google Agenda ficam concentradas na classe cloudia_calendar.py.

Utilizando os métodos nesta classe, somos capazes de:
- Consultar os dias e horários que o profissional está disponível;
- Verificar quais compromissos ele tem marcados;
- Agendar um compromisso de um usuário com um profissional;
- 

## Outros Arquivos e Classes Importantes:

`Context` ---> Nessa classe, guardamos todos os dados que dizem respeito a interação entre o usuário e a clínica. Por exemplo: quando o usuário seleciona uma data para agendar uma consulta, um horário, quando ele seleciona um profissional com o qual ele deseja realizar um agendamento, essas informações ficam armazenadas no Context.

`sentences.py` ---> Este arquivo guarda todas as frases ditas pela Cloudia ou outras frases que são importantes para ela reconhecer padrões.

`db.py` ---> arquivo onde vc seta as configurações do banco de dados que será utilizado pela Cloudia.

`config` ---> arquivo onde vc configura diversos parâmetros da Cloudia, incluindo a porta que ela roda na máquina que você vai executá-la

`appointment_lister` ---> worker que envia lembretes para os pacientes e resumos das agendas dos médicos.

`diretório mappings` ---> contém todas as tabelas do banco de dados em formato de objeto (ORM).

`facebook_comments_replier` ---> contém a classe que é responsável por responder aos comentários dos usuários nas páginas dos nossos clientes.

`diretório /bot/*` ---> contém as várias classes que controlam os bots de cada uma das páginas no Facebook.

`utils.py` ---> contém alguns métodos estáticos que ajudam outras classes do programa a executar algumas atividades específicas, como validação de e-mail e validação de CPF;

## Como começar?

`git clone https://<USER>@bitbucket.org/felipe_cloudia/cloudia4clinics.git`

Vá ao repositório do nosso chatbot (https://bitbucket.org/felipe_cloudia/cloudia4clinics/src/master/) e crie uma novo issue, descrevendo a atividade que você precisa desenvolver.

QUando você criar a issue, ele vai criar uma issue no repositório com um certo número. Exemplo: "Issue #26".

Se a Issue for uma correção de bug, você deve criar uma branch "fix/#26". Já se a issue for uma melhoria ou uma nova funcionalidade, você deve criar uma nova branch chamada "feat/#26". Essa nova branch deve ser criada a partir da branch master (ex. git checkout -b feat/#26) para implementar a sua funcionalidade. Siga [esse fluxogamara sugerido](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow). Ao final, você deve fazer um pull request.

Leia as instruções no README para saber como configurar o software na sua máquina. Caso você use outras configurações diferentes das nossas (outro SO, outra versão de SO), tente adaptar o README para o seu sistema e assim enriquecê-lo.

Se você utilizar o Docker, você conseguirá executar o chatbot facilmente em sua máquina. Mas você também pode executá-la localmente como explicado abaixo.

Fazer um banco local MySQL na sua máquina. Colocar os dados de um dump feito a partir do banco de produção. Deixar o MySQL server local sem password, executando o comando `mysqladmin -u root -p<password_atual_de_root> password '';`

Colocar a Cloudia para rodar na sua máquina, escolhendo uma porta para ela, que deve ser setada no arquivo `local.py` do diretório `config`;

Criar um webhook na sua máquina, usando preferencialmente o pagekite ou ngrok, apontando para a porta setada anteriormente no `config.py`;

Setar uma aplicação Chatbot de Facebook Messenger no Facebook Developers, colocar pelo menos uma página profissional sua pendurada nela, setar o webhook dela para o webhook que vc setou no passo anterior e adicionar os dados desta página no seu banco de dados local.

Selected events: messages, messaging_postbacks, messaging_optins, message_deliveries, message_reads, messaging_account_linking, message_echoes, feed